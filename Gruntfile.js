module.exports = function (grunt) {

	grunt.initConfig({

		meta: {
			src: 'src/main',
			test: 'src/test',
			bin: {
				instrument: 'bin/instrument',
				coverage: 'bin/coverage',
				reports: 'bin/coverage/reports'
			}
		},
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			files: {
				src: ['<%= meta.src %>/**/*.js']
			},
			gruntfile: {
				src: 'Gruntfile.js'
			}
		},
		watch: {
			lint: {
				files: '<%= jshint.files.src %>',
				tasks: 'jshint'
			},
			test: {
				files: ['<%=  meta.test %>/**/*.js'],
				tasks: ['jshint', 'jasmine_nodejs']
			}
		},
		nodemon: {
			dev: {
				script: '<%= meta.src%>/app.js',
				options: {
					ext: 'js,json'
				}
			}
		},
		concurrent: {
			target: {
				tasks: ['nodemon', 'watch'],
				options: {
					logConcurrentOutput: true
				}
			}
		},
		jasmine_nodejs: { // jshint ignore:line
			options: {
				specNameSuffix: "Test.js", // also accepts an array 
				helperNameSuffix: "Helper.js",
				useHelpers: true,
				stopOnFailure: true,
				reporters: {
					console: {
						colors: true,
						cleanStack: 1,       // (0|false)|(1|true)|2|3 
						verbosity: 4,        // (0|false)|1|2|3|(4|true) 
						listStyle: "indent", // "flat"|"indent" 
						activity: true
					}
				}
			},
			testing: {
				specs: [
					'<%= meta.test %>/**'
				],
				helpers: [
					'<%= meta.test %>/helper/**'
				]
			}
		},
		env: {
			coverage: {
				APP_DIR_FOR_CODE_COVERAGE: '../../../<%= meta.bin.instrument %>/src/main/'
			}
		},
		clean: {
			coverage: {
				src: ['<%= meta.bin.coverage %>']
			}
		},
		instrument: {
			files: ['<%= meta.src %>/**/*.js'],
			options: {
				lazy: true,
				basePath: '<%= meta.bin.instrument %>'
			}
		},
		storeCoverage: {
			options: {
				dir: '<%= meta.bin.reports %>'
			}
		},
		makeReport: {
			src: '<%= meta.bin.reports %>/**/*.json',
			options: {
				type: 'lcov',
				dir: '<%= meta.bin.reports %>',
				print: 'detail'
			}
		}

	});

	// plugins
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-jasmine-nodejs');
	grunt.loadNpmTasks('grunt-nodemon');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-istanbul');
	grunt.loadNpmTasks('grunt-env');

	// tasks
	grunt.registerTask('server', ['concurrent:target']);
	grunt.registerTask('test', ['jasmine_nodejs']);

	grunt.registerTask('coverage', ['jshint', 'clean', 'env:coverage',
		'instrument', 'test', 'storeCoverage', 'makeReport']);

	grunt.registerTask('default', ['coverage']);
};