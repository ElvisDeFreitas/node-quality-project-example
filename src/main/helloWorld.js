global.hello = function() {
  return 'world';
};
global.helloAsync = function(callback){
	setTimeout(callback('ok').bind(), 1000);
};