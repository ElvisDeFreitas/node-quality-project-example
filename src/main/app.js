var express = require('express');
var app = module.exports = express();

app.get("/", function(req, res) {
  res.send("Hello World");
});

var port = 6666;
app.listen(port);
console.log('listening on port ' + port);