// Include what we need to include: this is specific to jasmine-node
require2("helloWorld");

describe("hello", function() {
  it('returns "world"', function() {
    expect(hello()).toEqual("world");
  });
  it('returns "world2"', function() {
    expect(hello()).toEqual("world");
  });
  it('returns "world3"', function(done) {
    helloAsync(function(msg){
      expect("ok").toEqual(msg);
      done();
    });
  });
});

