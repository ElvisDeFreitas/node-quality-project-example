global.require2 = function (path) {
	/*
	 * APP_DIR_FOR_CODE_COVERAGE points to instrument path that integrate to istanbul to 
	 * cover integrated tests
	 */
	return require((process.env.APP_DIR_FOR_CODE_COVERAGE || '../../../src/main/') + path);
};
