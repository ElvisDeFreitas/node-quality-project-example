var request = require("supertest");
var app = require2('app');

describe("Hello World Server 2", function() {
	describe("GET /", function() {
		it("returns status code 200", function(done) {
			request(app)
				.get('/')
				.expect(200, done);
		});

		it("returns Hello World", function(done) {
			request(app)
				.get('/')
				.expect("Hello World", done);
		});
	});
});