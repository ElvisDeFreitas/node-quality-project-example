A project focused on quality with

* code quality analysis > [jshint](http://jshint.com/about/)
* unit tests > [jasmine](https://github.com/jasmine/jasmine)
* integrated tests > [jasmine](https://github.com/jasmine/jasmine)
* code coverage reports > [istanbul](https://github.com/gotwarlost/istanbul)
* task runner > [Gruntjs](http://gruntjs.com/)

# Usage

### Running tests and generating coverage

	npm run-script coverage

or

	grunt coverage

### Running only tests

	npm test

or

	grunt test

# Installation
	
	sudo npm install -g grunt-cli
	npm install

install only production dependencies

	npm install --production